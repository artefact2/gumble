/* Copyright 2018 Romain "Artefact2" Dal Maso <artefact2@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "coords.h"

static const cch_board_bitmap_t valid_n = ~RANK_8;
static const cch_board_bitmap_t valid_s = ~RANK_1;
static const cch_board_bitmap_t valid_w = ~FILE_A;
static const cch_board_bitmap_t valid_e = ~FILE_H;
static const cch_board_bitmap_t valid_nw = ~(RANK_8 | FILE_A);
static const cch_board_bitmap_t valid_ne = ~(RANK_8 | FILE_H);
static const cch_board_bitmap_t valid_sw = ~(RANK_1 | FILE_A);
static const cch_board_bitmap_t valid_se = ~(RANK_1 | FILE_H);

static const cch_board_bitmap_t valid_nnw = ~(RANK_7 | RANK_8 | FILE_A);
static const cch_board_bitmap_t valid_nne = ~(RANK_7 | RANK_8 | FILE_H);
static const cch_board_bitmap_t valid_ssw = ~(RANK_1 | RANK_2 | FILE_A);
static const cch_board_bitmap_t valid_sse = ~(RANK_1 | RANK_2 | FILE_H);
static const cch_board_bitmap_t valid_wwn = ~(RANK_8 | FILE_A | FILE_B);
static const cch_board_bitmap_t valid_wws = ~(RANK_1 | FILE_A | FILE_B);
static const cch_board_bitmap_t valid_een = ~(RANK_8 | FILE_G | FILE_H);
static const cch_board_bitmap_t valid_ees = ~(RANK_1 | FILE_G | FILE_H);

#define CCH_NORTH(sq) ((((~valid_n >> sq) & 1) << 7) | (sq + 1))
#define CCH_SOUTH(sq) ((((~valid_s >> sq) & 1) << 7) | (sq - 1))
#define CCH_WEST(sq) ((((~valid_w >> sq) & 1) << 7) | (sq - 8))
#define CCH_EAST(sq) ((((~valid_e >> sq) & 1) << 7) | (sq + 8))
#define CCH_NORTHWEST(sq) ((((~valid_nw >> sq) & 1) << 7) | (sq - 7))
#define CCH_NORTHEAST(sq) ((((~valid_ne >> sq) & 1) << 7) | (sq + 9))
#define CCH_SOUTHWEST(sq) ((((~valid_sw >> sq) & 1) << 7) | (sq - 9))
#define CCH_SOUTHEAST(sq) ((((~valid_se >> sq) & 1) << 7) | (sq + 7))

#define CCH_NNW(sq) ((((~valid_nnw >> sq) & 1) << 7) | (sq - 6))
#define CCH_NNE(sq) ((((~valid_nne >> sq) & 1) << 7) | (sq + 10))
#define CCH_SSW(sq) ((((~valid_ssw >> sq) & 1) << 7) | (sq - 10))
#define CCH_SSE(sq) ((((~valid_sse >> sq) & 1) << 7) | (sq + 6))
#define CCH_WWN(sq) ((((~valid_wwn >> sq) & 1) << 7) | (sq - 15))
#define CCH_WWS(sq) ((((~valid_wws >> sq) & 1) << 7) | (sq - 17))
#define CCH_EEN(sq) ((((~valid_een >> sq) & 1) << 7) | (sq + 17))
#define CCH_EES(sq) ((((~valid_ees >> sq) & 1) << 7) | (sq + 15))
