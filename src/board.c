/* Copyright 2018 Romain "Artefact2" Dal Maso <artefact2@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <assert.h>
#include "gumble.h"

void cch_init_board(cch_board_t* b) {
	cch_load_fen(b, "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
}

cch_game_state_t cch_game_state(cch_board_t* b) {
	if(b->smoves >= 50) {
		return CCH_DRAW_FIFTY_MOVE_RULE;
	}

	cch_movelist_t ml;
	unsigned char stop = cch_generate_moves(b, ml, CCH_LEGAL, 0, 64); /* XXX: pass as argument to avoid recomputation */
	if(stop == 0) {
		if(CCH_IS_OWN_KING_CHECKED(b)) {
			return b->side ? CCH_CHECKMATE_BLACK : CCH_CHECKMATE_WHITE;
		}
		return CCH_STALEMATE;
	}

	return b->side ? CCH_WHITE_TO_PLAY : CCH_BLACK_TO_PLAY;
}
