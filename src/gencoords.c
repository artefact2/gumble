/* Copyright 2018 Romain "Artefact2" Dal Maso <artefact2@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include "gumble.h"

static void diag(cch_piece_t start, cch_piece_t increment, cch_piece_t stop, char dr) {
	cch_piece_t ssq, esq;
	char f, r;
	for(ssq = start; ssq != stop; ssq += increment) {
		f = CCH_FILE(ssq);
		r = CCH_RANK(ssq);
		while(f < 7 && ((dr > 0 && r < 7) || (dr < 0 && r > 0))) {
			++f;
			r += dr;
		}
		esq = CCH_SQUARE(f, r);

		printf("#define DIAG_%c%c%c%c (0", 'A' + CCH_FILE(ssq), '1' + CCH_RANK(ssq), 'A' + CCH_FILE(esq), '1' + CCH_RANK(esq));

		f = CCH_FILE(ssq);
		r = CCH_RANK(ssq);
		while(f < 8 && r >= 0 && r < 8) {
			printf("|%c%c", 'A' + f, '1' + r);

			++f;
			r += dr;
		}

		fputs(")\n", stdout);
	}
}

int main(void) {
	cch_square_t file, rank, sq;
	for(file = 0; file < 8; ++file) {
		for(rank = 0; rank < 8; ++rank) {
			sq = CCH_SQUARE(file, rank);
			printf("#define SQ_%c%c %u\n", 'A' + file, '1' + rank, sq);
			printf("#define %c%c (1ULL << %u)\n", 'A' + file, '1' + rank, sq);
		}
	}

	for(file = 0; file < 8; ++file) {
		printf("#define FILE_%c (0", 'A' + file);
		for(rank = 0; rank < 8; ++rank) {
			printf("|%c%c", 'A' + file, '1' + rank);
		}
		fputs(")\n", stdout);
	}

	for(rank = 0; rank < 8; ++rank) {
		printf("#define RANK_%c (0", '1' + rank);
		for(file = 0; file < 8; ++file) {
			printf("|%c%c", 'A' + file, '1' + rank);
		}
		fputs(")\n", stdout);
	}

	/* diagonals that go "up" (from left to right) */
	diag(CCH_SQUARE(0, 0), CCH_SQUARE(0, 1) - CCH_SQUARE(0, 0), CCH_SQUARE(0, 7), 1);
	diag(CCH_SQUARE(1, 0), CCH_SQUARE(2, 0) - CCH_SQUARE(1, 0), CCH_SQUARE(7, 0), 1);

	/* diagonals that go "down" (from left to right) */
	diag(CCH_SQUARE(0, 7), CCH_SQUARE(0, 6) - CCH_SQUARE(0, 7), CCH_SQUARE(0, 0), -1);
	diag(CCH_SQUARE(1, 7), CCH_SQUARE(2, 7) - CCH_SQUARE(1, 7), CCH_SQUARE(7, 7), -1);

	return 0;
}
