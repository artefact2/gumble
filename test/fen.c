/* Copyright 2018 Romain "Artefact2" Dal Maso <artefact2@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include <string.h>
#include "gumble.h"
#include "coords.h"

#define CCH_MOVE(from, to) ((cch_move_t){ .start = from, .end = to, .promote = 0 })

static void assert_fen(const cch_board_t* b, const char* expected_fen) {
	char fen[SAFE_FEN_LENGTH];
	assert(cch_save_fen(b, fen, SAFE_FEN_LENGTH) == CCH_OK);
	assert(!strcmp(fen, expected_fen));
}

int main(void) {
	cch_board_t b;

	/* https://en.wikipedia.org/wiki/Forsyth%E2%80%93Edwards_Notation#Examples */

	cch_init_board(&b);
	assert_fen(&b, "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");

	assert(cch_play_legal_move(&b, &CCH_MOVE(SQ_E2, SQ_E4), 0) == CCH_OK);
	assert_fen(&b, "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1");

	assert(cch_play_pseudo_legal_move(&b, &CCH_MOVE(SQ_C7, SQ_C5), 0) == CCH_OK);
	assert_fen(&b, "rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq c6 0 2");

	assert(cch_play_move(&b, &CCH_MOVE(SQ_G1, SQ_F3), 0) == CCH_OK);
	assert_fen(&b, "rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 1 2");

	return 0;
}
